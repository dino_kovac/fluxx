class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :description
      t.string :file_file_name
      t.integer :file_file_size
      t.string :file_content_type
      t.boolean :public
      t.integer :user_id

      t.timestamps
    end
  end
end
