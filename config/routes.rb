Rails.application.routes.draw do
  devise_for :users
  resources :comments
  resources :photos

  resources :users, except: [:new, :destroy] do
    resources :photos do
      resources :comments, except: [:show, :update]
    end
  end

  root 'home#index'

  get '/search', to: 'users#search'

  get '/users/:id/friends', to: 'users#friends', as: :friends

  get '/users/:id/requests', to: 'users#requests', as: :requests

  post '/users/:id/befriend', to: 'users#befriend', as: :befriend

  post '/users/:id/accept', to: 'users#accept', as: :accept

  post '/users/:id/deny', to: 'users#deny', as: :deny

  post '/users/:user_id/photos/:id/like', to: 'photos#like', as: :like

  delete '/users/:user_id/photos/:id/dislike', to: 'photos#dislike', as: :dislike

  post '/users/:user_id/photos/:id/tag', to: 'photos#tag', as: :user_photo_taggings

  delete '/users/:user_id/photos/:id/untag/:tagged_user_id', to: 'photos#untag', as: :untag

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
