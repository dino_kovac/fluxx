class PhotosController < ApplicationController
  before_action :set_user
  before_action :set_photo, only: [:show, :edit, :update, :like, :dislike, :tag, :untag]
  before_action :check_permission, only: [:new, :create, :edit, :update]

  # GET /users
  # GET /users.json
  def index
    @photos = @user.photos
  end

  # GET /users/1
  # GET /users/1.json
  def show
    can_view
    @comments = @photo.comments.where('id is not null').newest_first
    @comment = @photo.comments.new
  end

  def new
    @photo = @user.photos.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @photo = @user.photos.new(photo_params)

    respond_to do |format|
      if @photo.save
        format.html { redirect_to [@user, @photo], notice: 'Photo was successfully uploaded.' }
        format.json { render :show, status: :created, location: [@user, @photo] }
      else
        format.html { render :new }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to [@user, @photo], notice: 'Photo was successfully uploaded.' }
        format.json { render :show, status: :ok, location: [@user, @photo] }
      else
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  def like
    like = @photo.likes.create(user_id: current_user.id)
    redirect_to request.referer
  end

  def dislike
    like = @photo.likes.find_by(user_id: current_user.id)
    like.destroy
    redirect_to request.referer
  end

  def tag
    tag = @photo.taggings.create(user_id: params[:tagging][:user_id])
    redirect_to request.referer
  end

  def untag
    tag = @photo.taggings.find_by(user_id: params[:tagged_user_id])
    tag.destroy
    redirect_to request.referer
  end

  private

    def can_view
      set_user
      set_photo
      redirect_to root_path unless @photo.can_view?(current_user)
    end

    def check_permission
      set_user
      redirect_to root_path unless current_user == @user
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:user_id])
    end

    def set_photo
      set_user
      @photo = @user.photos.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_params
      params.require(:photo).permit(:description, :file, :public)
    end
end
