class HomeController < ApplicationController

  def index
    @photos = Photo.all.newest_first.collect do |photo|
      photo if photo.can_view?(current_user)
    end
    @photos.compact!
  end

end