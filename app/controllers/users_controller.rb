class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :befriend, :requests, :accept, :deny, :friends]
  before_action :check_permission, only: [:edit, :update, :requests, :friends]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    if @user.is_friend?(current_user) || @user == current_user
      @photos = @user.photos.newest_first
    else
      @photos = @user.photos.where(public: true).newest_first
    end
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def search
    @users = User.where("first_name LIKE ? OR last_name LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%")
    render :index
  end

  def friends
    @users = current_user.all_friends
    render :index
  end

  def befriend
    @friendship = Friendship.create(user_id: current_user.id, friend_id: @user.id)
    flash[:notice] = 'Friend request sent'
    redirect_to request.referer
  end

  def accept
    friendship = Friendship.where(user_id: @user.id).first
    friendship.accepted = true
    friendship.save
    redirect_to request.referer
  end

  def deny
    friendship = Friendship.where(user_id: @user.id).first
    friendship.accepted = false
    friendship.save
    redirect_to request.referer
  end

  def requests
    @users = current_user.friend_requests
  end

  private

    def check_permission
      set_user
      redirect_to root_path unless current_user == @user
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name)
    end
end
