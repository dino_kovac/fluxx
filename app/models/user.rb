class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :comments
  has_many :photos
  has_many :friendships
  has_many :friends, through: :friendships

  has_many :inverse_friendships, class_name: "Friendship", foreign_key: "friend_id"
  has_many :inverse_friends, through: :inverse_friendships, source: :user

  scope :accepted, -> () { where("friendships.accepted = 1 OR friendships.accepted = 't'") }

  scope :not_accepted, -> () { where("friendships.accepted IS NULL OR friendships.accepted = 0 OR friendships.accepted = 'f'") }

  def friend_requests
    inverse_friends.not_accepted
  end

  def already_requested?(other_user)
    friends.not_accepted.include? other_user
  end

  def has_requested_friendship?(other_user)
    inverse_friends.not_accepted.include? other_user
  end

  def is_friend?(other_user)
    all_friends.include? other_user
  end

  def all_friends
    friends.accepted + inverse_friends.accepted
  end

  def name
    "#{first_name} #{last_name}"
  end
end
