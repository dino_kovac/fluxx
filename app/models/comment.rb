class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :photo

  scope :newest_first, -> { order('updated_at DESC') }
end
