class Photo < ActiveRecord::Base
  has_many :comments
  belongs_to :user

  has_many :likes
  has_many :like_users, through: :likes, source: :user

  has_many :taggings
  has_many :tagged_users, through: :taggings, source: :user

  has_attached_file :file
  validates_attachment_content_type :file, :content_type => /\Aimage\/.*\Z/

  scope :newest_first, -> { order('updated_at DESC') }

  def can_view?(other_user)
    user == other_user || user.is_friend?(other_user) || public
  end

  def has_liked?(other_user)
    like_users.include? other_user
  end

  def taggable_users
    User.all - tagged_users
  end
end
